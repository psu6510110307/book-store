import db from './db'

async function loadFixtures(clearData = false){
    if(clearData){
        console.warn('clearing data')
        await db('category').del()
    }
    await db.batchInsert('category', [
        {id: 1, title: 'Manga'},
        {id: 2, title: 'light novel'}
    ])
    
await db.batchInsert('book', [
    { id: 1, title: 'Spy room', price: 300, stockAmount: 100, categoryId: 2 },
    { id: 2, title: 'Bocchi the rock', price: 250, stockAmount: 60, categoryId: 1 },
    { id: 3, title: 'Bofuri Max Defense', price: 275, stockAmount: 40, categoryId: 2 },
  ])
}

export default loadFixtures